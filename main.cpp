
#include <cstdio>

#include <chrono>
#include <string>
#include <vector>

// clang-format will wrap the string below so it doesn't exceed the maximum line
// length. Although we have indentation set to use tabs, the alignment will use
// spaces to ensure it always lines up appropriately.
static_assert(__cplusplus >= 201402L, "This file must be compiled with C++14 or greater because we use a thousands separator (1'000) below, a feature unavailable in earlier versions");

using std::string;
using std::vector;

using HRClock = std::chrono::high_resolution_clock;
using TimePoint = std::chrono::time_point<HRClock>;
using TimeDuration = std::chrono::duration<int64_t, std::nano>;

TimePoint time_now() { return std::chrono::high_resolution_clock::now(); }

int main() {
	printf("Hello, world!\n");

	auto t1 = time_now();

      // clang-format will convert this indentation to tabs and give it consistent spacing
      vector< vector<int >>mat = { {1,2},{3, 4}  };

	// clang-format off
	mat[0][0] = 5; mat[0][1] = 6; // Disable clang-format where it makes sense to
	mat[1][0] = 7; mat[1][1] = 8;
	// clang-format on

	string table;
	for (const auto &row : mat) {
		for (const auto i : row) { table += std::to_string(i) + " "; }
		table += "\n";
	}

	// Memory leak. Uncomment to have it picked up by clang-tidy, scan-build, or AddressSanitizer
	// int *ii = new int;
	
	// Divide by Zero. Uncomment to have it picked up by clang-tidy or scan-build
	// int j = 6 / 0;

	auto t2 = time_now();
	TimeDuration exec_time = t2 - t1;
	int64_t microseconds = exec_time.count() / 1'000;
	printf("%s\n", table.c_str());
	printf("Done in %lld microseconds\n", microseconds);
}
