
## Install dependencies (Ubuntu 18.04)
```bash
sudo apt install clang-format clang-tidy clang-tools ninja-build
```

### Run clang-format
```bash
clang-format -i main.cpp
```

### Run clang-tidy
```bash
clang-tidy --warnings-as-errors=* main.cpp --
```

### Fix clang-tidy errors with the `--fix-errors` flag
```bash
clang-tidy --fix-errors main.cpp --
```

### Use `scan-build` to uncover more potential errors
It turns out scan-build checks are already included in clang-tidy. However:
```bash
scan-build c++ -std=c++17 -o main main.cpp
```

### Use sanitizers to uncover potential run-time errors
```bash
c++ -g -std=c++17 -fsanitize=address -o main main.cpp
./main
c++ -g -std=c++17 -fsanitize=thread -o main main.cpp
./main
c++ -g -std=c++17 -fsanitize=undefined -o main main.cpp
./main
```

### Build and run
```bash
c++ -std=c++17 -o main main.cpp
./main
```

### Use CMake to generate a `build.ninja` file
```bash
mkdir build
cd build
cmake -GNinja ..
ninja
./cppsample
```

## Windows (Visual Studio 19)
https://docs.microsoft.com/en-us/cpp/code-quality/clang-tidy?view=vs-2019
https://docs.microsoft.com/en-us/cpp/build/cmake-projects-in-visual-studio?view=vs-2019#ide-integration

Open folder in Visual Studio.

### Build and run
- Right-click CMakeLists.txt -> "Generate Cache for cppsample"
- Debug -> Start cppsample

### Run clang-format
- Tools -> Options -> Text Editor -> C/C++ -> Formatting -> General -> ClangFormat execution -> Run ClangFormat for all formatting scenarios
- Edit -> Advanced -> Format Document

## Run clang-tidy
- Analyze -> Run Code Analysis -> Run Code Analysis on cppsample
